# Solr Video Annotator

Connects to a Solr instance, crawls the documents, and adds relevant video links to the Solr Index pointing to the original documents.

Intra-PARC Dependencies:
1. [pyMetaVideoLib](https://gitlab.com/rocket-public/pymetavideolib)
1. [pyProperNounFinderLib](https://gitlab.com/rocket-public/pypropernounfinderlib)

## Installation


## Operations
