#!/bin/bash

activate() {
		source ./VENV/bin/activate
		pip3 install wheel
		pip3 install -r requirements.txt
}

rm -rf VENV
python3 -m venv VENV
activate
