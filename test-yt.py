from pymetavideolib import YouTube_handler
from pprint import PrettyPrinter

google_dev_key = ''
product = 'G2260 printer'
pp = PrettyPrinter()

yt = YouTube_handler(google_dev_key)
results_list = yt.get_videos(keyword=product,
                     part='snippet', 
                     type='video', 
                     caption='any', 
                     embeddable='any', 
                     maxResults=1)

for item in results_list:
    print('=========================================================')
    pp.pprint(item)

