from pypropernounfinderlib import *

text = "All I want for Christmas is a Xerox WorkCentre 7345 and a cup of coffee."
print("Text direct test")
print(find_proper_nouns_in_text(text))
print("Text file test")
print(find_proper_nouns_in_text_file("testtext.txt"))

html = "<html><head></head><body>I really love my Canon PIXMA G2260 printer</body></html>"
print("HTML direct test")
print(find_proper_nouns_in_html(html))
print("HTML file test")
print(find_proper_nouns_in_html_file("printing.html"))


